const BASE_URL = "https://62db6cb9d1d97b9e0c4f348b.mockapi.io";
let edit_id = "";
getDSSV();

function getDSSV() {
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      renderDSSV(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}
function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      getDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}
function editSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      showThongTinLenForm(res.data);
      edit_id = res.data.id;
    })
    .catch(function (err) {
      console.log(err);
    });
}
function updateSinhVien() {
  axios({
    url: `${BASE_URL}/sv/${edit_id}`,
    method: "PUT",
    data: layThongTinTuForm(),
  })
    .then(function () {
      getDSSV();
    })
    .catch(function (err) {
      console.log(err.response);
    });
}
function themSinhVien() {
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: layThongTinTuForm(),
  })
    .then(function () {
      getDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}
