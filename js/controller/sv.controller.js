let renderDSSV = function (dssv) {
  let contentHTML = "";
  dssv.forEach((sv) => {
    let contentTr = `<tr>
        <td>${sv.id}</td>
        <td>${sv.name}</td>
        <td>${sv.email}</td>
        <td>
        <button onClick="xoaSinhVien(${sv.id})" class="btn btn-danger">Xoa</button>
        <button onClick="editSinhVien(${sv.id})" class="btn btn-warning">Sua</button>
        </td>
        </tr>`;
    contentHTML += contentTr;
  });
  document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
};
let layThongTinTuForm = function () {
  const id = document.querySelector("#txtMaSV").value;
  const name = document.querySelector("#txtTenSV").value;
  const email = document.querySelector("#txtEmail").value;
  const math = document.querySelector("#txtDiemToan").value;
  const physics = document.querySelector("#txtDiemLy").value;
  const chemistry = document.querySelector("#txtDiemHoa").value;

  return {
    id: id,
    name: name,
    email: email,
    math: math,
    physics: physics,
    chemistry: chemistry,
  };
};
let showThongTinLenForm = function (sinhVien) {
  document.querySelector("#txtMaSV").value = sinhVien.id;
  document.querySelector("#txtTenSV").value = sinhVien.name;
  document.querySelector("#txtEmail").value = sinhVien.email;
  document.querySelector("#txtDiemToan").value = sinhVien.math;
  document.querySelector("#txtDiemLy").value = sinhVien.physics;
  document.querySelector("#txtDiemHoa").value = sinhVien.chemistry;
};
